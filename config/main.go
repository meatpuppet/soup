package config

import (
	"fmt"
	"os"
	"strconv"

	"github.com/joho/godotenv"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var Config configuration

type configuration struct {
	SecretKey          string
	Loglevel           string
	ConcurrentCrawlers int

	RedisAddress  string
	RedisDB       int
	RedisPassword string

	ManticoreUser     string
	ManticorePassword string
	ManticoreHost     string
	ManticoreDB       string

	MinimumTextLength int
	MaxSteps          int
	Languages         []string
}

func (c *configuration) GetQueueIds() []string {
	ids := []string{}
	for id := 0; id < Config.ConcurrentCrawlers; id++ {
		ids = append(ids, fmt.Sprint(id))
	}
	return ids
}

func init() {
	var secretKey string
	var redisAddress string
	var redisPassword string
	var redisDB int
	var loglevel string
	var manticoreUser string
	var manticorePassword string
	var manticoreHost string
	var manticoreDB string
	var concurrentCrawlers int
	var maxSteps int

	var minimumTextLength int
	var languages []string

	if os.Getenv("SOUP_TESTING") == "1" {
		secretKey = "secret"
		redisAddress = "localhost:6379"
		redisPassword = ""
		redisDB = 1

		manticoreUser = "manticore"
		manticorePassword = "manticore"
		manticoreHost = "localhost:9306"
		manticoreDB = "testing"

		loglevel = "debug"
		concurrentCrawlers = 20
		minimumTextLength = 150
		maxSteps = 1
		languages = append(languages, "de", "en")
	} else {
		envFilePath := os.Getenv("ENV_FILE_PATH")
		if envFilePath == "" {
			envFilePath = ".env"
		}

		log.Debug().Msgf("loading env file from %s", envFilePath)
		err := godotenv.Load(envFilePath)
		if err != nil {
			log.Warn().Msgf("Error loading env file from %s", envFilePath)
		}

		secretKey = os.Getenv("SECRET_KEY")
		redisAddress = os.Getenv("REDIS_ADDRESS")
		redisPassword = os.Getenv("REDIS_PASSWORD")
		manticoreUser = os.Getenv("MANTICORE_USER")
		manticorePassword = os.Getenv("MANTICORE_PASSWORD")
		manticoreHost = os.Getenv("MANTICORE_HOST")
		manticoreDB = os.Getenv("MANTICORE_DB")

		redisDB, err = strconv.Atoi(os.Getenv("REDIS_DB"))
		if err != nil {
			panic(err)
		}
		loglevel = os.Getenv("LOGLEVEL")
		concurrentCrawlers, err = strconv.Atoi(os.Getenv("CONCURRENT_CRAWLERS"))
		if err != nil {
			panic(err)
		}
		maxSteps, err = strconv.Atoi(os.Getenv("MAX_STEPS"))
		if err != nil {
			panic(err)
		}
		minimumTextLength = 150
		languages = append(languages, "de", "en")
	}

	Config = configuration{
		SecretKey:          secretKey,
		RedisAddress:       redisAddress,
		RedisDB:            redisDB,
		RedisPassword:      redisPassword,
		Loglevel:           loglevel,
		ManticoreUser:      manticoreUser,
		ManticorePassword:  manticorePassword,
		ManticoreHost:      manticoreHost,
		ManticoreDB:        manticoreDB,
		ConcurrentCrawlers: concurrentCrawlers,
		MinimumTextLength:  minimumTextLength,
		Languages:          languages,
		MaxSteps:           maxSteps,
	}

	log.Info().Msgf("starting with config: %v", Config)

	setupLogger()
}

func setupLogger() {
	level, err := zerolog.ParseLevel(Config.Loglevel)
	if err != nil {
		panic(err)
	}
	zerolog.SetGlobalLevel(level)
}
