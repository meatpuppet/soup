package manticore

import (
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/rs/zerolog/log"
)

type SearchResult struct {
	Title            string
	Sitename         string
	BaseUrl          string
	HighlightTitle   string
	HighlightContent string
	Id               int
	Score            int
	Tier             int
	Author           string
	Path             string
	Date             time.Time
	LastUpdate       time.Time
}

func (c *clientWrapper) Search(pattern string, language string) []*SearchResult {
	sql := fmt.Sprintf(`
    select 
        id,
        weight(),
        title,
        snippet(title, ?) as highlight_title,
        snippet(content, ?) as highlight_content,
        date,
        tier,
        base_url,
        path,
        author,
        sitename,
        last_update
    from pages_de, pages_en
    where
        match(?)
    `)
	rows, err := c.Db.Query(sql, pattern, pattern, pattern)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var searchResults []*SearchResult

	for rows.Next() {
		var result SearchResult
		if err := rows.Scan(
			&result.Id,
			&result.Score,
			&result.Title,
			&result.HighlightTitle,
			&result.HighlightContent,
			(*ManticoreTime)(&result.Date),
			&result.Tier,
			&result.BaseUrl,
			&result.Path,
			&result.Author,
			&result.Sitename,
			(*ManticoreTime)(&result.LastUpdate),
		); err != nil {
			log.Error().Msgf("error querying: %s", err)
			return nil
		}
		log.Debug().Msgf("got %s %s", result.BaseUrl, result.Path)
		searchResults = append(searchResults, &result)
	}
	if err = rows.Err(); err != nil {
		return searchResults
	}
	return searchResults
}
