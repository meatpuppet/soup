package manticore

import (
	"fmt"

	"codeberg.org/meatpuppet/soup/urldata"

	_ "github.com/go-sql-driver/mysql"
	"github.com/rs/zerolog/log"
    "time"
)

func (c *clientWrapper) InsertResult(urlData *urldata.UrlData) int64 {
	sql := fmt.Sprintf(`
        replace into pages_%s (
            id,
            title, 
            content,
            date,
            tier,
            base_url,
            path,
            author,
            sitename,
            content_md5,
            last_update
        )
        values (
            ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
        )
    `, urlData.Language)

    id := ToManticoreId(urlData.UrlHash)
	log.Debug().Msgf("adding %d, %s", id, urlData.Url)

	res, err := c.Db.Exec(sql,
		id,
		urlData.Title,
		urlData.Text,
		urlData.Date.Unix(),
		urlData.Tier,
		urlData.BaseUrl,
		urlData.ParsedUrl.RequestURI(),
		urlData.Author,
		urlData.Sitename,
		ToManticoreId(urlData.ContentHash),
        time.Now().Unix(),
	)

	if err != nil {
		panic(err)
	}
    lastId, err := res.LastInsertId()
	if err != nil {
		panic(err)
	}
    return lastId
}
