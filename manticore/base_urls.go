package manticore

import (
	"github.com/rs/zerolog/log"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

type BaseUrlResult struct {
    BaseUrl string
    Count int
}

func (c *clientWrapper) RemoveByBaseUrl(baseUrl string) {
	sql := `
        delete from pages_de, pages_en
        where
          base_url = ?
    `
	log.Warn().Msgf("removing base url %s", baseUrl)
	_, err := c.Db.Exec(sql,
		baseUrl,
	)
	if err != nil {
		panic(err)
	}
}

// a list of base urls, used as an overview to whats crawled
func (c *clientWrapper) GetBaseUrls(page int, perPage int, filterPattern string) []*BaseUrlResult {
	first := (page - 1) * perPage
    log.Debug().Msgf("getting base urls: page %d, perPage %d first %d filter %s", page, perPage, first, filterPattern)
	var rows *sql.Rows
    var err error
	if filterPattern != "" {
		queryString := `select 
            base_url, 
            count(*) as count
            from pages_de, pages_en
                where regex(base_url, ?)
            group by base_url 
            order by count desc 
            limit ?,?
        `
		rows, err = c.Db.Query(queryString, filterPattern, first, perPage)
		if err != nil {
			panic(err)
		}
	} else {
    log.Debug().Msg("query without filter!")
		queryString := `select 
            base_url, 
            count(*) as count
            from pages_de, pages_en
            group by base_url 
            order by count desc 
            limit ?,?
        `
		rows, err = c.Db.Query(queryString, first, perPage)
		if err != nil {
			panic(err)
		}
	}
    defer rows.Close()

    searchResults := []*BaseUrlResult{}
	for rows.Next() {
        log.Debug().Msgf("got result")
		var result BaseUrlResult
		if err := rows.Scan(
			&result.BaseUrl,
			&result.Count,
		); err != nil {
			log.Error().Msgf("error querying: %s", err)
			return nil
		}
        log.Debug().Msgf("appending %v", result)
		searchResults = append(searchResults, &result)
	}
	if err = rows.Err(); err != nil {
        log.Error().Msgf("error %s", err)
		return searchResults
	}
	return searchResults
}
