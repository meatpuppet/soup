package manticore_test

import (
	"testing"

	"codeberg.org/meatpuppet/soup/manticore"
	"codeberg.org/meatpuppet/soup/urldata"
)

func TestInsertResult(t *testing.T){
    urlData, _ := urldata.NewUrlData("http://test.de", 1, 1)
    urlData.Language = "en"

    w := manticore.ClientWrapper() 
    id := w.InsertResult(urlData)

    id2 := w.InsertResult(urlData)

    if id != id2 {
        t.Logf("id %d != id2 %d", id, id2)
        t.Fail()
    }
}

