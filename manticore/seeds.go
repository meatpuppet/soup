package manticore

import (
	"bufio"
	"database/sql"
	"os"
	"time"

	"codeberg.org/meatpuppet/soup/urldata"

	"github.com/rs/zerolog/log"
)

func (c *clientWrapper) createSeeds() error {
	sql := `
    create table seeds(
        base_url string, 
        path string, 
        date timestamp, 
        tier int, 
        steps int,
        comment text
    )`
	_, err := c.Db.Exec(sql)

	if err != nil {
		return err
	}
	return nil
}

type SeedUrl struct {
	Id      int
	BaseUrl string
	Path    string
	Date    time.Time
	Tier    int
	Steps   int
	Comment string
}

func (c *clientWrapper) AddSeedUrl(urlData *urldata.UrlData, tier int, steps int, comment string) bool {
	sql := `
        insert into seeds (
            id,
            base_url, 
            path,
            date,
            tier,
            steps,
            comment
        )
        values (
            ?, ?, ?, ?, ?, ?, ?
        )
    `
    id := ToManticoreId(urlData.UrlHash)
    log.Info().Msgf("adding url %s (hash %d)", urlData.BaseUrl + urlData.ParsedUrl.RequestURI(), id)

	_, err := c.Db.Exec(sql,
		id,
		urlData.BaseUrl,
		urlData.ParsedUrl.RequestURI(),
		time.Now().Unix(),
		tier,
		steps,
		comment,
	)
	if err != nil {
		panic(err)
	}

	//affectedCount, err := res.RowsAffected()
	//if err != nil {
	//	panic(err)
	//}
	// ~~1 means one row inserted, >1 means there were rows deleted before add~~
	// ~~https://dev.mysql.com/doc/refman/8.0/en/replace.html~~
	// ...but it looks like its always 1 on manticore
	//wasInserted := (affectedCount == 1)
	//log.Debug().Msgf("affected rows was %d -> wasInserted %t", affectedCount, wasInserted)
	return true

}

func (c *clientWrapper) RemoveSeedUrl(id int) error {
	sql := `
        delete from seeds
        where
            id = ?
    `
	_, err := c.Db.Exec(sql,
		id,
	)
	if err != nil {
		return err
	}
	return nil
}

func (c *clientWrapper) GetSeedUrls(page int, perPage int, filterPattern string) []*SeedUrl {
	first := (page - 1) * perPage
	log.Debug().Msgf("getting seed urls: page %d, perPage %d first %d filter %s", page, perPage, first, filterPattern)
	var rows *sql.Rows
	var err error
	if filterPattern != "" {
		queryString := `select 
            id,
            base_url, 
            path,
            date,
            tier,
            steps,
            comment
        from seeds
            where regex(base_url, ?) or regex(path, ?)
        order by base_url desc 
        limit ?,?
        `
		rows, err = c.Db.Query(queryString, filterPattern, filterPattern, first, perPage)
		if err != nil {
			panic(err)
		}
	} else {
		log.Debug().Msg("query without filter!")
		queryString := `select 
            id,
            base_url, 
            path,
            date,
            tier,
            steps,
            comment
        from seeds
        order by base_url desc 
        limit ?,?
        `
		rows, err = c.Db.Query(queryString, first, perPage)
		if err != nil {
			panic(err)
		}
	}
	defer rows.Close()

	seedUrlResults := []*SeedUrl{}
	for rows.Next() {
		var result SeedUrl
		if err := rows.Scan(
			&result.Id,
			&result.BaseUrl,
			&result.Path,
			(*ManticoreTime)(&result.Date),
			&result.Tier,
			&result.Steps,
			&result.Comment,
		); err != nil {
			log.Error().Msgf("error querying: %s", err)
			return nil
		}
		seedUrlResults = append(seedUrlResults, &result)
	}
	if err = rows.Err(); err != nil {
		log.Error().Msgf("error %s", err)
	}
	return seedUrlResults
}

func (c *clientWrapper) Backup() {
	// create file
	f, err := os.Create("backup/seeds.txt")
	if err != nil {
		log.Fatal().Msgf("%s", err)
	}
	// remember to close the file
	defer f.Close()

	// create new buffer
	buffer := bufio.NewWriter(f)

	var cursor uint64
	for {
		seeds := c.GetSeedUrls(1, 10000, "")
		for _, seed := range seeds {
			_, err := buffer.WriteString(seed.BaseUrl + seed.Path + "\n")
			if err != nil {
				log.Fatal().Msgf("%s", err)
			}
		}
		// flush buffered data to the file
		log.Debug().Msg("flushing to file...")
		if err := buffer.Flush(); err != nil {
			log.Fatal().Msgf("%s", err)
		}
		if cursor == 0 {
			break
		}
	}
}
