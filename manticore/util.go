package manticore

import (
	"strconv"
	"time"
)

type ManticoreTime time.Time

// feels like a horrible hack, stolen from https://github.com/mattn/go-sqlite3/issues/190#issuecomment-343341834
// scan a mysql timestamp into a go time.Time
func (t *ManticoreTime) Scan(v interface{}) error {
	// Should be more strictly to check this type.
	// log.Debug().Msgf("parsing %s", v)
    timestamp, err := strconv.Atoi(string(v.([]uint8)))
    if err != nil {
        panic(err)
    }
	vt := time.Unix(int64(timestamp), 0)
	*t = ManticoreTime(vt)
	return nil
}
