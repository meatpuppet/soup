package manticore

import (
	"codeberg.org/meatpuppet/soup/config"
	"fmt"

	"github.com/rs/zerolog/log"

	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

type clientWrapper struct {
	Db *sql.DB
}

func ToManticoreId(i uint64) int64 {
    return int64(i >> 1)
}

func FromManticoreId(i int64) uint64 {
    return uint64(i << 1)
}

func ClientWrapper() *clientWrapper {
	//db, err := sql.Open("mysql", "user7:s$cret@tcp(127.0.0.1:3306)/testdb")
	// interpolateParams=true makes go fill in the params in query instead of the db,
	// since manticore does not support that
	// see https://github.com/go-sql-driver/mysql/issues/360
	// and https://github.com/go-sql-driver/mysql/blob/5a8a207333b3cbdd6f50a31da2d448658343637e/README.md#interpolateparams
	hostAddress := fmt.Sprintf("%s:%s@tcp(%s)/%s?interpolateParams=true&parseTime=true", config.Config.ManticoreUser, config.Config.RedisPassword, config.Config.ManticoreHost, config.Config.ManticoreDB)
	db, err := sql.Open("mysql", hostAddress)

	if err != nil {
		log.Fatal().Msgf("%s", err)
	}

	return &clientWrapper{db}
}

func (c *clientWrapper) Init() error {
	err := c.createPages()
	if err != nil {
		log.Warn().Msgf("error creating manticore index: %s", err)
	}
	err = c.createSeeds()
	if err != nil {
		log.Warn().Msgf("error creating manticore index: %s", err)
	}
	return nil
}

func (c *clientWrapper) createPages() error {
	for _, lang := range config.Config.Languages {
		sql := fmt.Sprintf(`
        create table pages_%s(
            title text, 
            content text, 
            date timestamp, 
            tier int, 
            base_url string, 
            path string, 
            author string, 
            sitename string, 
            content_md5 string,
            last_update timestamp
        ) morphology = 'lemmatize_%s_all'`, lang, lang)
		_, err := c.Db.Exec(sql)

		if err != nil {
            log.Error().Msgf("error creating index: %s", err)
		}
	}
	return nil
}
