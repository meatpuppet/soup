#! /bin/bash

mkdir -p lemmatizers
cd lemmatizers
curl https://repo.manticoresearch.com/repository/morphology/de.pak.tgz | tar xvz
curl https://repo.manticoresearch.com/repository/morphology/en.pak.tgz | tar xvz
cd ..

