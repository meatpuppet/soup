/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"encoding/json"
	"fmt"

	"codeberg.org/meatpuppet/soup/crawler/content_extraction"
	"codeberg.org/meatpuppet/soup/fetch"
	"codeberg.org/meatpuppet/soup/urldata"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

func getUrl(url string) {
	url = "https://github.com/MicrosoftDocs/microsoft-365-docs/issues?q=is%3Aissue+comments%3A%3E50+comments%3A%3E50+updated%3A%3E2022-03-29+updated%3A%3C2022-03-01+updated%3A%3E2022-03-29+updated%3A%3E2022-03-29+comments%3A%3E50+updated%3A%3C2022-03-01+updated%3A%3E2022-03-29+is%3Aclosed+updated%3A%3C2022-03-01+comments%3A%3E50+comments%3A%3E50+comments%3A%3E50+linked%3Apr+no%3Amilestone+comments%3A%3E50+-label%3Abug+no%3Aassignee+comments%3A%3E50+sort%3Acreated-desc+updated%3A%3C2022-03-01"
    urlData, err := urldata.NewUrlData(url, 1, 1)
	if err != nil {
		log.Error().Msgf("error parsing: %s", err)
	}

	cleanUrl, content, err := fetch.Fetch(url)
	if err != nil {
		log.Error().Msgf("error fetching: %s", err)
	}

    contentextraction.Extract(urlData, content)
	if err != nil {
		log.Error().Msgf("error extracting: %s", err)
	}

	fmt.Println("got " + cleanUrl.String())
	fmt.Println("urldata:")
    s, _ := json.MarshalIndent(urlData, "", "\t")
    fmt.Print(string(s))
}

// getUrlCmd represents the getUrl command
var getUrlCmd = &cobra.Command{
	Use:  "getUrl",
	Args: cobra.MinimumNArgs(1),

	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		getUrl(args[0])
	},
}

func init() {
	rootCmd.AddCommand(getUrlCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getUrlCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getUrlCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
