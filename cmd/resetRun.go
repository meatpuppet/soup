/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"fmt"

	"codeberg.org/meatpuppet/soup/redis"
	"github.com/spf13/cobra"
)

// resetRunCmd represents the resetRun command
var resetRunCmd = &cobra.Command{
	Use:   "resetRun",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("dropping temp data...")
        redis.Wrapper().CleanDb()
	},
}

func init() {
	rootCmd.AddCommand(resetRunCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// resetRunCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// resetRunCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
