package urldata

import (
	"encoding/json"
	"fmt"
	"regexp"
)

// base url configs
// used to restrict crawling
type BaseUrlConfig struct {
	// how many url we want to crawl on a base base url
	Budget int `json:"budget"`

    ExcludePatterns []string `json:"exclude_patterns"` // exclude patterns to match on the normalized url
}

func (b *BaseUrlConfig) MatchesExcludePattern(url string) bool {
    for _, pattern := range b.ExcludePatterns {
        if len(regexp.MustCompile(pattern).FindString(url)) > 0 {
            return true
        }
    }
    return false
}


func DefaultBaseUrlConfig() *BaseUrlConfig {
	baseUrlConfig := BaseUrlConfig{}
	baseUrlConfig.Budget = 5000
    baseUrlConfig.ExcludePatterns = []string{}

	return &baseUrlConfig
}

func BaseUrlConfigFromJson(baseUrlConfigJson []byte) *BaseUrlConfig {
	baseUrlConfig := BaseUrlConfig{}

	err := json.Unmarshal(baseUrlConfigJson, &baseUrlConfig)
	if err != nil {
        panic(err)
	}
	return &baseUrlConfig
}

func (baseUrlConfig *BaseUrlConfig) ToBaseUrlConfigJson() []byte {
	j, err := json.Marshal(baseUrlConfig)
	if err != nil {
		panic(fmt.Sprintf("cannot marshal baseUrlConfig: %+v", baseUrlConfig))
	}
	return j
}
