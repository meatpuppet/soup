package urldata_test

import (
	"codeberg.org/meatpuppet/soup/urldata"
	"codeberg.org/meatpuppet/soup/config"
	"testing"
)

func TestGetQueueId(t *testing.T) {
	type testdata struct {
		in  uint64
		out string
	}
	expected := []testdata{
		{1, "1"},
		{19, "19"},
		{20, "0"},
	}

	config.Config.ConcurrentCrawlers = 20

	urlData, _ := urldata.NewUrlData("https://test.tld", 1, 1)
	for _, testcase := range expected {
		urlData.BaseUrlHash = testcase.in
		if !(urlData.GetQueueId() == testcase.out) {
			t.Logf("should have been %s, was %s", testcase.out, urlData.GetQueueId())
			t.Fail()
		}
	}
}

func TestGetQueueIds(t *testing.T) {
	config.Config.ConcurrentCrawlers = 2
	ids := config.Config.GetQueueIds()
	expected := []string{"0", "1"}
    if len(ids) != len(expected) {
        t.Logf("len %d != len %d", len(ids), len(expected))
        t.Logf("array was %s", ids)
        t.Fail()
    }
	for i, id := range ids {
		if id != expected[i] {
			t.Fail()
		}
	}

}
