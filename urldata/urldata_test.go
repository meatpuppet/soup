package urldata_test

import (
	"codeberg.org/meatpuppet/soup/urldata"
	"testing"
)

func TestHexConvert(t *testing.T) {
	expected := []uint64{1, 100, 99999999}

	for _, testcase := range expected {
        t.Logf("test %d", testcase)
		hexStr := urldata.GetHexString(testcase)
        t.Logf("hex %s", hexStr)
		i := urldata.HexStrToInt(hexStr)
		if i != testcase {
			t.Logf("should have been %d, was %d", testcase, i)
			t.Fail()
		}
	}
}
