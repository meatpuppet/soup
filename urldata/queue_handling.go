package urldata

import (
    "fmt"
    "codeberg.org/meatpuppet/soup/config"
)

func (urlData *UrlData) GetQueueId() string {
    return fmt.Sprint(int(urlData.BaseUrlHash) % config.Config.ConcurrentCrawlers)
}
