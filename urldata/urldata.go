package urldata

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"time"
    "codeberg.org/meatpuppet/soup/detect_language"
	"github.com/cespare/xxhash"
	"github.com/goware/urlx"
	"github.com/markusmobius/go-trafilatura"
)

// used to store url in redis
type urlDataTodo struct {
	Url  string `json:"url"`
	Tier int    `json:"tier"`

	// steps to different hosts left for this url
	Steps int `json:"steps"`
}

// used to store wrap the url data while working with it, as well as the fetched results
type UrlData struct {
	Url   string
	Tier  int
	Steps int

	BaseUrl string

	ParsedUrl *url.URL

	UrlHash     uint64
	BaseUrlHash uint64

	ContentHash   uint64
	Text          string
	InternalLinks []string // a list of links
	ExternalLinks []string // a list of links

	Title    string
	Author   string
	Sitename string
	Date     time.Time
	Language string
}

func GetHexString(i uint64) string {
   return fmt.Sprintf("%x", i)
}

func HexStrToInt(s string) uint64 {
    i, err := strconv.ParseInt(s, 16, 64)
    if err != nil {
        panic(err)
    }
    return uint64(i)
}

func GetBaseUrl(url *url.URL) string {
	return fmt.Sprintf("%s://%s", url.Scheme, url.Host)
}

// create new UrlData
func NewUrlData(url string, tier int, steps int) (*UrlData, error) {

	urlData := UrlData{}
	urlData.Tier = tier
	urlData.Steps = steps

    err := urlData.InitFromUrl(url)

	return &urlData, err
}

func (urlData *UrlData) InitFromUrl(url string) error {
	parsedUrl, err := urlx.ParseWithDefaultScheme(url, "https")
	if err != nil {
		return err
	}
	// remove fragments, like #some_anchor
	parsedUrl.Fragment = ""

	baseUrl := GetBaseUrl(parsedUrl)
	url = baseUrl + parsedUrl.RequestURI()

    urlData.Url = url
	urlData.BaseUrl = baseUrl

	urlData.ParsedUrl = parsedUrl

    urlData.UrlHash = xxhash.Sum64String(url)
    urlData.BaseUrlHash =  xxhash.Sum64String(baseUrl)

    return nil
}


func (urlData *UrlData) GetUrlHashStr() string {
    return GetHexString(urlData.UrlHash)
}

func (urlData *UrlData) GetBaseUrlHashStr() string {
    return GetHexString(urlData.BaseUrlHash)
}

func (urlData *UrlData) GetContentHashStr() string {
    return GetHexString(xxhash.Sum64String(urlData.Text))
}

func UrlDataFromTodoJson(todoJson []byte) (*UrlData, error) {
	todoData := urlDataTodo{}
	err := json.Unmarshal(todoJson, &todoData)
	if err != nil {
		return nil, err
	}
	return NewUrlData(todoData.Url, todoData.Tier, todoData.Steps)
}

func (urlData *UrlData) ToTodoJson() []byte {
	todoData := urlDataTodo{urlData.Url, urlData.Tier, urlData.Steps}
	j, err := json.Marshal(todoData)
	if err != nil {
		panic(fmt.Sprintf("cannot marshal todoData: %+v", todoData))
	}
	return j
}

func (urlData *UrlData) SetContent(extractResult *trafilatura.ExtractResult) {
    language, can_detect := detectlanguage.DetectLanguage(extractResult.ContentText)
    if !can_detect {
        language = ""
    }
    urlData.Language = language
    urlData.Text = extractResult.ContentText
    urlData.ContentHash = xxhash.Sum64String(urlData.Text)
    urlData.Title = extractResult.Metadata.Title
    urlData.Author = extractResult.Metadata.Author
    urlData.Sitename = extractResult.Metadata.Sitename
    urlData.Date = extractResult.Metadata.Date
}

/*
* try to follow the url,
* update url and md5 sums if changed
 */
func (urlData *UrlData) TryUpdateUrl() error {
	// check connection, upgrade to https if 301
	res, err := http.Head(urlData.Url)
	if err != nil {
		return err
	}
	if res.StatusCode < 200 || res.StatusCode >= 300 {
		return errors.New(fmt.Sprintf("wrong statuscode: %s", res.Status))
	}
	finalUrl := res.Request.URL.String()
	if finalUrl != urlData.Url {
        err := urlData.InitFromUrl(finalUrl)
        if err != nil {
            return err
        }
	}
	return nil
}
