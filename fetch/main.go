package fetch

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	nurl "net/url"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
	"codeberg.org/meatpuppet/soup/version"
)

func Fetch(url string) (*url.URL, []byte, error) {
    httpClient := &http.Client{Timeout: 30 * time.Second}

    log.Debug().Msgf("fetching %s", url)
	// Prepare URL
    _, err := nurl.ParseRequestURI(url)
	if err != nil {
		return nil, nil, errors.New(fmt.Sprintf("failed to parse url: %v", err))
	}

	// Fetch article
    req, err := http.NewRequest("GET", url, nil)
    if err != nil {
        log.Error().Msgf("error constructing request for %s", url)
        return nil, nil, err
    }
    req.Header.Set("user-agent", "soupbot/v" + version.Version)

	resp, err := httpClient.Do(req)
	if err != nil {
		log.Error().Msgf("failed to fetch the page: %v", err)
        return nil, nil, err
	}
	defer resp.Body.Close()

    if resp.StatusCode <= 200 && resp.StatusCode >= 299 {
        return nil, nil, errors.New(fmt.Sprintf("got status code %d: %s", resp.StatusCode, resp.Status))
    }

    // contentLength := resp.Header.Get("content-length")

    // limit content lengts to 1mb 
    contentType := resp.Header.Get("content-type")

    // content type can have things like ; charset... appended
    if !strings.HasPrefix(contentType, "text/") {
        return nil, nil, errors.New(fmt.Sprintf("wrong content type '%s'", contentType))
    } else {
        //log.Debug().Msgf("got content type %s", contentType)
    }
    body := io.LimitReader(resp.Body, 1024*1024)
    buffer := new(bytes.Buffer)
	buffer.ReadFrom(body)
    // return the actual url (after redirect) along with the content
    return resp.Request.URL, buffer.Bytes(), nil
}
