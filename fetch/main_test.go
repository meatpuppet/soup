package fetch_test

import (
	"fmt"
	//"crypto/rand"
	"net/http"
	"net/http/httptest"
	"testing"
    "codeberg.org/meatpuppet/soup/fetch"
)

func TestFetchLimit(t *testing.T) {
	expectedBytes := 1024 * 1024
	generated := expectedBytes * 3

	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")

		bytes := make([]byte, generated)
		fmt.Fprintln(w, bytes)
	}))
	defer svr.Close()

	_, receivedBytes, err := fetch.Fetch(svr.URL)
	if err != nil {
        t.Logf("error fetching %s", err)
		t.Fail()
	}

	if len(receivedBytes) != expectedBytes {
		t.Logf("received %d bytes", len(receivedBytes))
		t.Fail()
	}
}
