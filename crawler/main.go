package crawler

import (
	"fmt"
	"time"

	"codeberg.org/meatpuppet/soup/blocklist"
	"codeberg.org/meatpuppet/soup/config"
	"codeberg.org/meatpuppet/soup/crawler/robots_txt"
	"codeberg.org/meatpuppet/soup/crawler/urlcheck"
	"codeberg.org/meatpuppet/soup/manticore"
	"codeberg.org/meatpuppet/soup/redis"
	"codeberg.org/meatpuppet/soup/urldata"

	"github.com/rs/zerolog/log"
)

func Init() {
	wrapper := redis.Wrapper()
	wrapper.FlushDb()

	manticoreWrapper := manticore.ClientWrapper()
	manticoreWrapper.Init()

	defer manticoreWrapper.Db.Close()
}


// called before adding urls to todo list
// the intention is to do some cheap checks before adding to the list
// (more expensive stuff like fetching the robots before actual processing)
func urlLooksFetchable(comingFrom *urldata.UrlData, toAdd *urldata.UrlData, robots *robotstxt.RobotsTxt, baseUrlConfig *urldata.BaseUrlConfig) bool {
	// todo: steal checks from pythons courlan
	if !urlcheck.CheckUrl(toAdd) {
		return false
	}

	if comingFrom.BaseUrlHash == toAdd.BaseUrlHash {
		if robots.IsDisallowed(toAdd.ParsedUrl) {
			return false
		}
		if baseUrlConfig.MatchesExcludePattern(toAdd.Url) {
			return false
		}
	}
	return true
}

func Reset() {
	wrapper := redis.Wrapper()
	wrapper.CleanDb()
	log.Info().Msg("sleeping")
	time.Sleep(10 * time.Second)

	manticoreWrapper := manticore.ClientWrapper()

	page := 1
	perPage := 100
	for {
		seedUrls := manticoreWrapper.GetSeedUrls(page, perPage, "")
		if len(seedUrls) == 0 {
			break
		}
		page += 1
		for _, seedUrl := range seedUrls {
			url := fmt.Sprintf("%s%s", seedUrl.BaseUrl, seedUrl.Path)
			todoUrl, err := urldata.NewUrlData(url, seedUrl.Tier, seedUrl.Steps)
			if err != nil {
				log.Error().Msgf("cant create urldata for %s: %s", url, err)
			}
			wrapper.AddToTodo(todoUrl, false)
		}
	}
}


func Run() {
	manticoreWrapper := manticore.ClientWrapper()
	defer manticoreWrapper.Db.Close()
	
    redisWrapper := redis.Wrapper()

	resultChan := make(chan urldata.UrlData)
	queueEmpty := make(chan bool)

    // connect redis blocklist updates to blocklist.BlockRules
	blocklistUpdates := make(chan string)
    go blocklist.BlockRules.WaitForUpdate(blocklistUpdates)
    go redisWrapper.SubscribeBlocklistUpdate(blocklistUpdates)
   
    // start crawlers
    for _, id := range config.Config.GetQueueIds() {
		go crawl(string(id), resultChan, queueEmpty)
	}
        
    // receive results
	for {
		select {
		case result := <-resultChan:
			manticoreWrapper.InsertResult(&result)
		case _ = <-queueEmpty:
			if redisWrapper.AllQueuesEmpty() {
				Reset()
			}
		}
	}
}
