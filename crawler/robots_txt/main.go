package robotstxt

import (
	"fmt"
	"net/url"
	"codeberg.org/meatpuppet/soup/fetch"
	"codeberg.org/meatpuppet/soup/urldata"

	"github.com/rs/zerolog/log"
    "github.com/temoto/robotstxt"
)

type RobotsTxt struct {
    robotsChecker *robotstxt.RobotsData
    RobotsTxtData []byte
}

func (r RobotsTxt) IsDisallowed(url *url.URL) bool {
     return !r.robotsChecker.TestAgent(url.Path, "soupbot")
}

func FromBytes(b []byte) *RobotsTxt {
    robots, err := robotstxt.FromBytes([]byte(b))
    robotsTxt := RobotsTxt{robots, b}
    if err != nil {
        log.Error().Msgf("cant parse robots.txt: %s", err)
        robots, _ := robotstxt.FromBytes([]byte(""))
        return &RobotsTxt{robots, b}
    }
    return &robotsTxt
}

func Fetch(urlData *urldata.UrlData) *RobotsTxt{
    robotsUrl := fmt.Sprintf("%s/%s", urlData.BaseUrl, "robots.txt")
    _, resp, err := fetch.Fetch(robotsUrl) 
    if err != nil {
        log.Warn().Msgf("cant find robots.txt at %s", robotsUrl)
        log.Warn().Msgf("error was: %s", err)
        robots, _ := robotstxt.FromBytes([]byte(""))
        return &RobotsTxt{robots, []byte("")}
    }
    log.Info().Msgf("got robots.txt from %s", robotsUrl)
    robots, err := robotstxt.FromBytes(resp)
    if err != nil {
        log.Error().Msgf("cant parse robots.txt: %s", err)
        robots, _ = robotstxt.FromBytes([]byte(""))
    }
    return &RobotsTxt{robots, resp}
}

