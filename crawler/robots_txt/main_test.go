package robotstxt_test

import (
	"net/url"
	"testing"

	"codeberg.org/meatpuppet/soup/crawler/robots_txt"
)


func TestIsDisallowed(t *testing.T) {
    
	robotsTxtString := `
User-Agent: *
Disallow: /test/
    `
	robots := robotstxt.FromBytes([]byte(robotsTxtString))
    parsedUrl, _ := url.Parse("http://test.tld/test/abcd.html")
    if !robots.IsDisallowed(parsedUrl) {
        t.Fail()
    }
}
