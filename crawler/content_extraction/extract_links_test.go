package contentextraction_test

import (
	"net/url"
	"reflect"
	"testing"

	"codeberg.org/meatpuppet/soup/crawler/content_extraction"
)

func TestExtract(t *testing.T) {
	testContent := `
        <html>
        <head></head>
        <body>
            <a href="internal.html"></a>
            <a href="./internal.html"></a>
            <a href="./internal"></a>
            <a href="//somewhereelse.com/external.html"></a>
            <a href="http://somewhereelse.com/external.html"></a>
            <a href="https://somewhereelse.com/external.html"></a>
        </body>
        </html>
        `
    requestedUrl := "https://somedomain.com"
    expectedInternal := []string{
        "https://somedomain.com/internal.html",
        "https://somedomain.com/internal.html",
        "https://somedomain.com/internal",
    }
    expectedExternal := []string{
        "https://somewhereelse.com/external.html",
        "http://somewhereelse.com/external.html",
        "https://somewhereelse.com/external.html",
    }
    parsedUrl, _ := url.Parse(requestedUrl)
	internalLinks, externalLinks := contentextraction.ExtractLinks(parsedUrl, []byte(testContent))
    if !reflect.DeepEqual(internalLinks, expectedInternal) {
        t.Log("internal not equal")
        for n := range expectedInternal {
            t.Logf("expected %s, got %s", expectedInternal[n], internalLinks[n])

        }
        t.Fail()
    }
    if !reflect.DeepEqual(externalLinks, expectedExternal) {
        t.Log("External not equal")
        for n := range expectedExternal {
            t.Logf("expected %s, got %s", expectedExternal[n], externalLinks[n])

        }
        t.Fail()
    }
}
