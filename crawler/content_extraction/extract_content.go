package contentextraction

import (
	"io"
	"net/url"

	"github.com/markusmobius/go-trafilatura"
)

func extractContent(url *url.URL, body io.Reader) (*trafilatura.ExtractResult, error) {
	// Extract content
	opts := trafilatura.Options{
		IncludeImages:   false,
		ExcludeComments: true,
		OriginalURL:     url,
	}
	return trafilatura.Extract(body, opts)
}
