package contentextraction

import (
	"io"
	"io/ioutil"
	nurl "net/url"
	"regexp"

	"github.com/goware/urlx"
	"github.com/rs/zerolog/log"
)

// based on https://gist.github.com/darrenxyli/604bb27228bb06121f74
func extractLinks(url *nurl.URL, body io.Reader) ([]string, []string, error) {
	var (
		err           error
		internalLinks []string = make([]string, 0)
		externalLinks []string = make([]string, 0)
		matches       [][]string
		findLinks     = regexp.MustCompile("<a.*?href=\"(.*?)\"")
	)

	content, err := ioutil.ReadAll(body)
	if err != nil {
		panic(err)
	}

	//log.Debug().Msgf("parsing links...")
	//log.Debug().Msgf("%s", content)
	// Retrieve all anchor tag URLs from string
	matches = findLinks.FindAllStringSubmatch(string(content), -1)

	for _, val := range matches {
		var linkUrl *nurl.URL

		// Parse the anchr tag URL
		linkUrl, err := url.Parse(val[1])
		if err != nil {
			log.Error().Msgf("error parsing link: %s", val[1])
			continue
		}

		if linkUrl.Scheme == "" {
			linkUrl.Scheme = url.Scheme
		}

		// If the URL is absolute, add it to the slice
		// If the URL is relative, build an absolute URL
		if linkUrl.IsAbs() {
			if linkUrl.Scheme == "http" || linkUrl.Scheme == "https" {
				// test if its an internal link if neccessary
				isSameBaseUrl := (url.Scheme == linkUrl.Scheme && url.Host == linkUrl.Host)
				normalizedLinkUrl, err := urlx.Normalize(linkUrl)
				if err != nil {
					log.Warn().Msgf("could not normalize %s", linkUrl.String())
				} else {
					if isSameBaseUrl {
						internalLinks = append(internalLinks, normalizedLinkUrl)
					} else {
						externalLinks = append(externalLinks, normalizedLinkUrl)
					}
				}
			}
		} else {
			// relative links should always be internal..
			linkUrl.Scheme = url.Scheme
			linkUrl.Host = url.Host
			normalizedLinkUrl, err := urlx.Normalize(linkUrl)
			if err != nil {
				log.Warn().Msgf("could not normalize %s", linkUrl.String())

			} else {
				internalLinks = append(internalLinks, normalizedLinkUrl)
			}
		}
	}

	return internalLinks, externalLinks, err
}
