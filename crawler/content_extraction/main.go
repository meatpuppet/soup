package contentextraction

import (
	"bytes"
	"errors"
	"net/url"

	"codeberg.org/meatpuppet/soup/config"
	"codeberg.org/meatpuppet/soup/urldata"
)

func ExtractLinks(url *url.URL, contents []byte) ([]string, []string) {
	internalLinks, externalLinks, err := extractLinks(url, bytes.NewReader(contents))
    if err != nil {
        return []string{}, []string{}
    }
    return internalLinks, externalLinks
}


func Extract(urlData *urldata.UrlData, contents []byte) error {
    extractedContent, err := extractContent(urlData.ParsedUrl, bytes.NewReader(contents))
	if err != nil {
		return err
	}

    if len(extractedContent.ContentText) > config.Config.MinimumTextLength {
        urlData.SetContent(extractedContent)
    } else {
        return errors.New("text too short")
    }
	return nil
}

