// test if a url looks crawl-worthy (aka: is it text and not an image or so)

package urlcheck

import (
	"regexp"
	"codeberg.org/meatpuppet/soup/urldata"
	"strings"

	"github.com/rs/zerolog/log"
)

var validEndings = []string{
    // from courlan whitelist
	".adp",
	".amp",
	".asp",
	".aspx",
	".cfm",
	".cgi",
	".do",
	".htm",
	"html",
	".jsp",
	".mht",
	".mhtml",
	".php",
	".php3",
	".php4",
	".php5",
	".phtml",
	".pl",
	".shtml",
	".stm",
	".txt",
	".xhtml",
	".xml",
	".md",
}
var validRegex = []string{
	"^$",        // empty path
	`^([^.]+)$`, // match if no file extension https://stackoverflow.com/questions/18017677/regex-for-a-file-name-without-an-extension
}

func checkSuffix(path string) bool {
	for _, ending := range validEndings {
		if strings.HasSuffix(string(path), ending) {
			return true
		}
	}
	return false
}

func checkRegex(path string) bool {
	for _, regex := range validRegex {
		isMatch, err := regexp.Match(regex, []byte(path))
		if err != nil {
			// compile error
			panic(err)
		}
		if isMatch {
			return true
		}
	}
	return false
}

func CheckUrl(url *urldata.UrlData) bool {
	if !checkSuffix(url.ParsedUrl.Path) && !checkRegex(url.ParsedUrl.Path) {
        log.Debug().Msgf("hit suffix (%t) or regex (%t) check", checkSuffix(url.ParsedUrl.Path), checkRegex(url.ParsedUrl.Path))
		return false
	}
    if !basicFilter(url.Url) {
        log.Debug().Msg("hit basic filter check")
        return false
    }
    if !spamFilter(url.Url) {
        log.Debug().Msg("hit spam filter check")
        return false
    }
    if notCrawlableFilter(url.Url) {
        log.Debug().Msg("hit not crawlable filter check")
        return false
    }
    /*if !typeFilter(url.Url, false) {
       return false
    }*/
	return true

}
