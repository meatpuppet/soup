// add stuff from courlan/filters.py
// https://github.com/adbar/courlan

package urlcheck

import (
	"regexp"
	"strings"

	"github.com/rs/zerolog/log"
)

var ignoreCase = "(?i)"

// content filters
var wordpress_content_filter = regexp.MustCompile(`/(?:page|seite|user|search|gallery|gall?erie|labels|archives|uploads|modules|attachment)/|/(?:tags?|schlagwort|category|cat|kategorie|kat|auth?or)/[^/]+/?$`)

var param_filter = regexp.MustCompile(`\.(atom|json|css|xml|js|jpg|jpeg|png|gif|tiff|pdf|ogg|mp3|m4a|aac|avi|mp4|mov|webm|flv|ico|pls|zip|tar|gz|iso|swf)\b`)

var adult_filter = regexp.MustCompile(`\b(?:adult|amateur|arsch|cams?|cash|fick|gangbang|incest|porn|sexyeroti[ck]|sexcam|swinger|xxx|bild\-?kontakte)\b`)

var navigation_filter = regexp.MustCompile(ignoreCase + "/(archives|auth?or|cat|category|kat|kategorie|page|schlagwort|seite|tags?|topics?|user)/")

var notcrawlable = regexp.MustCompile(ignoreCase + `/(login|impressum|imprint)(\.[a-z]{3,4})?/?$|/login\?|/(javascript:|mailto:|tel\.?:|whatsapp:)`)

func typeFilter(url string, strict bool) bool {
	// Make sure the target URL is from a suitable type (HTML page with primarily text)

	if strings.HasSuffix(url, "/feed") {
		log.Debug().Msg("ends with feed")
		return false
	}
	if strings.HasSuffix(url, "/rss") {
		log.Debug().Msg("ends with rss")
		return false
	}
	// embedded content
	if len(regexp.MustCompile(ignoreCase+"/oembed\b").FindString(url)) > 0 {
		return false
	}
	// wordpress structure
	if wordpress_content_filter.MatchString(url) {
		if !isNavigationPage(url) {
			return false
		}
	}

	// hidden in parameters
	if strict && param_filter.MatchString(url) {
		return false
	}
	// not suitable
	// if regexp.MatchString(ignoreCase+"https?://banner\.|https?://add?s?\.", url) {
	if regexp.MustCompile(ignoreCase + "https?://banner.|https?://add?s?.").MatchString(url) {
		return false
	}
	// search..?
	if len(regexp.MustCompile(ignoreCase+"\b(?:doubleclick|tradedoubler|livestream)\b|/(?:live|videos?)/").FindString(url)) > 0 {
		return false
	}
	if strict && len(regexp.MustCompile(ignoreCase+"\b(?:live|videos?)\b").FindString(url)) > 0 {
		return false
	}
	return true
}

// stuff like impressum, login etc
func notCrawlableFilter(url string) bool {
	return len(notcrawlable.FindString(url)) > 0
}

// Determine if the URL is related to navigation and overview pages
// rather than content pages, e.g. /page/1 vs. article page.
func isNavigationPage(url string) bool {
	return len(navigation_filter.FindString(url)) > 0
}

// Try to filter out spam and adult websites
func spamFilter(url string) bool {
	if len(adult_filter.FindString(url)) > 0 {
		return false
	}
	return true
}

// Filter URLs based on basic formal characteristics
func basicFilter(url string) bool {
	if !strings.HasPrefix(url, "http") || len(url) >= 500 || len(url) < 10 {
		return false
	}
	return true
}

/* language filter
# |/(www\.)?(facebook\.com|google\.com|instagram\.com|twitter\.com)/
INDEX_PAGE_FILTER = re.compile(r'.{0,5}/index(\.[a-z]{3,4})?/?$', re.IGNORECASE)

# territories whitelist
# see also: https://babel.pocoo.org/en/latest/api/languages.html
# get_official_languages('ch')
LANGUAGE_MAPPINGS = {
    'de': {'at', 'ch', 'de', 'li'},  # 'be', 'it'
    'en': {'au', 'ca', 'en', 'gb', 'ie', 'nz', 'us'},
    'fr': {'be', 'ca', 'ch', 'fr', 'tn'},  # , 'lu', ...
}


def path_filter(urlpath, query):
    '''Filters based on URL path: index page, imprint, etc.'''
    if NOTCRAWLABLE.search(urlpath):
        return False
    if INDEX_PAGE_FILTER.match(urlpath) and len(query) == 0:
        #print('#', urlpath, INDEX_PAGE_FILTER.match(urlpath), query)
        return False
    return True

    return True


def type_filter(url, strict=False, with_nav=False):
    '''Make sure the target URL is from a suitable type (HTML page with primarily text)'''
    # directory
    #if url.endswith('/'):
    #    return False
    try:
        # feeds
        if url.endswith(('/feed', '/rss')):
            raise ValueError
        # embedded content
        if re.search(r'/oembed\b', url, re.IGNORECASE):
            raise ValueError
        # wordpress structure
        if WORDPRESS_CONTENT_FILTER.search(url):
            if with_nav is not True or not is_navigation_page(url):
                raise ValueError
        # hidden in parameters
        if strict is True and PARAM_FILTER.search(url):
            raise ValueError
        # not suitable
        if re.match(r'https?://banner\.|https?://add?s?\.', url, re.IGNORECASE):
            raise ValueError
        if re.search(r'\b(?:doubleclick|tradedoubler|livestream)\b|/(?:live|videos?)/', url, re.IGNORECASE):
            raise ValueError
        if strict is True and re.search(r'\b(?:live|videos?)\b', url, re.IGNORECASE):
            raise ValueError
    except ValueError:
        return False
    # default
    return True


def validate_url(url):
    '''Parse and validate the input'''
    try:
        parsed_url = urlparse(url)
    except ValueError:
        return False, None
    if bool(parsed_url.scheme) is False or parsed_url.scheme not in ('http', 'https'):
        return False, None
    if len(parsed_url.netloc) < 5 or \
       (parsed_url.netloc.startswith('www.') and len(parsed_url.netloc) < 8):
        return False, None
    # if validators.url(parsed_url.geturl(), public=True) is False:
    #    return False
    # default
    return True, parsed_url


def is_navigation_page(url):
    '''Determine if the URL is related to navigation and overview pages
       rather than content pages, e.g. /page/1 vs. article page.'''
    return bool(NAVIGATION_FILTER.search(url))


def is_not_crawlable(url):
    '''Run tests to check if the URL may lead to deep web or pages
       generally not usable in a crawling context.'''
    return bool(NOTCRAWLABLE.search(url))
*/
