package urlcheck

import (
    "testing"
    "net/url"
)


var testTypeFilterStringsRegex = []struct {
	in  *url.URL
	out bool
}{
	{getUrl("http://test.de"), true},
	{getUrl("http://test.de"), true},
	{getUrl("http://test.de/something"), true},
	{getUrl("http://test.de/something?bla"), true},
	{getUrl("http://test.de/something/page/?p=1"), true},
}


func TestTypeFilter(t *testing.T) {
	for _, testString := range testTypeFilterStringsRegex {
		if typeFilter(testString.in.Path, true) != testString.out {
            t.Logf("check failed: %s != %t", testString.in, testString.out)
			t.Fail()
		}
	}
}

var testSpamFilterStringsRegex = []struct {
	in  *url.URL
	out bool
}{
	{getUrl("http://test.de"), true},
	{getUrl("http://test.de/something?bla"), true},
	{getUrl("http://test.de/something/porn/?p=1"), false},
}


func TestSpamFilter(t *testing.T) {
	for _, testString := range testSpamFilterStringsRegex {
		if spamFilter(testString.in.Path) != testString.out {
            t.Logf("check failed: %s != %t", testString.in, testString.out)
			t.Fail()
		}
	}
}
