package urlcheck // same package, so that we can test private functions

import (
	"net/url"
	"testing"
)

func getUrl(s string) *url.URL {
	url, err := url.Parse(s)
	if err != nil {
		panic(err)
	}
	return url
}

var testStringsSuffix = []struct {
	in  *url.URL
	out bool
}{
	{getUrl("http://test.de/something.html"), true},
	{getUrl("http://test.de/something.htm"), true},
	{getUrl("http://test.de/something.txt"), true},
	{getUrl("http://test.de/something.md"), true},
	{getUrl("http://test.de/something.php"), true},
}

func TestCheckSuffix(t *testing.T) {
	for _, testString := range testStringsSuffix {
		if checkSuffix(testString.in.Path) != testString.out {
            t.Logf("check failed: %s != %t", testString.in, testString.out)
			t.Fail()
		}
	}
}

var testStringsRegex = []struct {
	in  *url.URL
	out bool
}{
	{getUrl("http://test.de"), true},
	{getUrl("http://test.de"), true},
	{getUrl("http://test.de/something"), true},
	{getUrl("http://test.de/something?bla"), true},
	{getUrl("http://test.de/something.html"), false},
	{getUrl("http://test.de/something.xyz"), false},
}

func TestCheckRegex(t *testing.T) {
	for _, testString := range testStringsRegex {
		if checkRegex(testString.in.Path) != testString.out {
            t.Logf("check failed: %s != %t", testString.in, testString.out)
			t.Fail()
		}
	}
}

var testStringsNotCrawlable = []struct {
	in  *url.URL
	out bool
}{
	{getUrl("http://test.de"), true},
	{getUrl("http://test.de"), true},
	{getUrl("http://test.de/login"), false},
	{getUrl("http://test.de/login/"), false},
	{getUrl("http://test.de/impressum/"), false},
	{getUrl("http://test.de/something?bla"), true},
	{getUrl("http://test.de/something/1"), true},
	{getUrl("http://test.de/something.xyz"), true},
    {getUrl("https://criticalmass.in/bremen"), true},
}

func TestNotCrawlable(t *testing.T) {
	for _, testString := range testStringsRegex {
		if checkRegex(testString.in.Path) != testString.out {
            t.Logf("check failed: %s != %t", testString.in, testString.out)
			t.Fail()
		}
	}
}
