// a single crawler, connected to redis
package crawler

import (
	"time"

	"codeberg.org/meatpuppet/soup/config"
	"codeberg.org/meatpuppet/soup/crawler/content_extraction"
	"codeberg.org/meatpuppet/soup/crawler/robots_txt"
	"codeberg.org/meatpuppet/soup/fetch"
	"codeberg.org/meatpuppet/soup/redis"
	"codeberg.org/meatpuppet/soup/blocklist"
	"codeberg.org/meatpuppet/soup/urldata"
	"github.com/rs/zerolog/log"
)

// get the robots.txt from redis - fetch from base url if not found
func getOrFetchRobotsTxt(wrapper *redis.RedisWrapper, urlData *urldata.UrlData) *robotstxt.RobotsTxt {
	robotsTxt, err := wrapper.GetRobotsTxt(urlData)
	if err != nil {
		robotsTxt = robotstxt.Fetch(urlData)
		wrapper.SetRobotsTxt(urlData, robotsTxt)
	}
	return robotsTxt
}

func processTier(
	wrapper *redis.RedisWrapper,
	comingFromUrl *urldata.UrlData,
	newUrlData *urldata.UrlData,
) {
	if comingFromUrl.BaseUrlHash == newUrlData.BaseUrlHash {
		// same domain - same tier
		newUrlData.Tier = comingFromUrl.Tier
	} else {
		// different domain - count up tier
		newUrlData.Tier = comingFromUrl.Tier + 1

		// check what tier we have in redis
		tier, err := wrapper.GetBaseUrlTier(comingFromUrl)
		if err != nil {
			// we havent seen this yet - set what we calculated
			wrapper.SetBaseUrlTier(newUrlData, newUrlData.Tier)
		} else if newUrlData.Tier > tier {
			// we have a shorter path to this base url in redis
			newUrlData.Tier = tier
		} else {
			// newUrlData.Tier < tier
			// we found a shorter path than what we have in redis - update!
			// comingFrom: update in manticore as well
			wrapper.SetBaseUrlTier(newUrlData, tier)
		}
	}
}

func tryAddLinkToTodo(redisWrapper *redis.RedisWrapper, comingFromUrlData, newUrlData *urldata.UrlData, robotsTxt *robotstxt.RobotsTxt, baseUrlConfig *urldata.BaseUrlConfig, isInternal bool) {

	// some checks before adding
	if urlLooksFetchable(comingFromUrlData, newUrlData, robotsTxt, baseUrlConfig) {

		// check if base url is excluded,
		// or if the base url is not the same (meaning we are on a non-excluded domain rn)
		if (newUrlData.BaseUrl != comingFromUrlData.BaseUrl) || !blocklist.BlockRules.IsBlocked(newUrlData.Url) {

			// set tier for urlData
			processTier(redisWrapper, comingFromUrlData, newUrlData)
			log.Debug().Msgf("adding %s", comingFromUrlData.Url)
			redisWrapper.AddToTodo(newUrlData, isInternal)
		} else {
			log.Debug().Msgf("%s does not look fetchable", newUrlData.Url)
		}
	} else {
		log.Debug().Msgf("%s does not look fetchable", newUrlData.Url)
	}
}

func processLinks(wrapper *redis.RedisWrapper, todoUrlData *urldata.UrlData, result []byte, robotsTxt *robotstxt.RobotsTxt, baseUrlConfig *urldata.BaseUrlConfig) {
	internalLinks, externalLinks := contentextraction.ExtractLinks(todoUrlData.ParsedUrl, result)
	for _, link := range internalLinks {
		isInternal := true
		newUrlData, err := urldata.NewUrlData(link, 0, todoUrlData.Steps)
		if err != nil {
			log.Info().Msgf("something is wrong with %v: %s", link, err)
		} else {
			tryAddLinkToTodo(wrapper, todoUrlData, newUrlData, robotsTxt, baseUrlConfig, isInternal)
		}
	}
	// if we have steps left for this host, process external links too
	if todoUrlData.Steps >= 1 {
		for _, link := range externalLinks {
			isInternal := false
			newUrlData, err := urldata.NewUrlData(link, 0, todoUrlData.Steps-1)
			if err != nil {
				log.Info().Msgf("something is wrong with %v: %s", link, err)
			} else {
				// add new link to redis
				tryAddLinkToTodo(wrapper, todoUrlData, newUrlData, robotsTxt, baseUrlConfig, isInternal)
			}
		}
	}
}

// main crawl function: operates entirely on redis, returns urldata.UrlData to store text to manticore in a channel
func crawl(queueId string, resultChannel chan<- urldata.UrlData, queueEmpty chan<- bool) {
	log.Debug().Msgf("starting crawler on queue %s", queueId)
	wrapper := redis.Wrapper()
	for {
		urlHash, err := wrapper.PopFromTodoList(queueId)
		if err != nil {
			// we hit the end of the queue - restart with fresh seed!
			seconds := 10
			log.Info().Msgf("we hit the end of the %s todo list!", queueId)
			log.Info().Msgf("queue %s sleeping %ds...", queueId, seconds)
			queueEmpty <- true
			time.Sleep(time.Duration(seconds) * time.Second)
			continue
		}

		todoUrlData, err := wrapper.GetTodoUrlData(urlHash)
		if err != nil {
			log.Error().Msg("skipping todo - broken")
			continue
		}
		// move to done before we hit errors - we dont want to crawl broken stuff again
		wrapper.MoveToDone(todoUrlData)

		log.Info().Msgf("processing %v", todoUrlData.Url)

		/*
		 * get config and state of the base url
		 *
		 * todo:
		 * check if domain is broken (did we get non-2xx errors before?)
		 *
		 * test urls if there is a https version - and use that instead?
		 *
		 * fetch rss/atom (eg. https://www.heise.de/download/feed/top) only for getting links, not content..?
		 */

		isExcluded := blocklist.BlockRules.IsBlocked(todoUrlData.Url)
		if isExcluded {
			log.Info().Msgf("base url is excluded: %s", todoUrlData.Url)
			continue
		}

		baseUrlConfig := wrapper.GetBaseUrlConfig(todoUrlData)
		robotsTxt := getOrFetchRobotsTxt(wrapper, todoUrlData)

		/*
		 * skip if we dont want to crawl this url for some reason
		 */
		if robotsTxt.IsDisallowed(todoUrlData.ParsedUrl) {
			// skip if robots.txt says this is a disallowed url
			log.Info().Msgf("skipping %s, because robots", todoUrlData.Url)
			continue
		}
		if baseUrlConfig.MatchesExcludePattern(todoUrlData.Url) {
			log.Info().Msgf("skipping, matches exclude pattern")
			continue
		}

		/*
		 * we do want to fetch the url - so finally actually do it!
		 */
		// increase budget before fetch
		usedBudget := wrapper.IncreaseUsedBudget(todoUrlData)
		if usedBudget >= baseUrlConfig.Budget {
			log.Info().Msgf("budget of %d is used up for %s, skipping", baseUrlConfig.Budget, todoUrlData.Url)
			continue
		}
		log.Info().Msgf("fetching %s (tier %d)", todoUrlData.Url, todoUrlData.Tier)
		updatedUrl, result, err := fetch.Fetch(todoUrlData.Url)

		if err != nil {
			log.Warn().Msgf("error fetching: %s", err)
		} else {
			// try update url (on redirect)
			if updatedUrl.String() != todoUrlData.ParsedUrl.String() {
                todoUrlData, err = urldata.NewUrlData(updatedUrl.String(), todoUrlData.Tier, todoUrlData.Steps)
                if err != nil {
                    log.Error().Msgf("error updating url %s", updatedUrl.String())
                }
				// if we got redirected - set this domain to done as well!
				wrapper.MoveToDone(todoUrlData)

				if blocklist.BlockRules.IsBlocked(todoUrlData.Url) {
					log.Info().Msgf("updated to excluded base url, skipping: %s", todoUrlData.Url)
					continue
				}
			}
			err := contentextraction.Extract(todoUrlData, result)
			if err != nil {
				// something went wrong extracting content - skip!
				log.Error().Msgf("could not extract content from %s: %s", todoUrlData.Url, err)
				continue
			}
			
            processLinks(wrapper, todoUrlData, result, robotsTxt, baseUrlConfig)

			for _, language := range config.Config.Languages {
				if todoUrlData.Language == language {
					resultChannel <- *todoUrlData
				} else {
                    log.Debug().Msgf("not adding result, language was %s", todoUrlData.Language)
                }
			}
		}
	}
}
