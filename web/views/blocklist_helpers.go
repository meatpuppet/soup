package views

import (
	"fmt"
	"strings"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"

	"net/http"
)

func addToBlocklist(c *gin.Context, blockRules string) {
	session := sessions.Default(c)
	for _, rule := range strings.Split(blockRules, "\n") {
		if rule != "" {
			rule = strings.TrimSpace(rule)

			// todo: reload blocklist in crawler somehow
            err := redisWrapper.AddToBlocklist(rule)
            if err != nil {
                session.AddFlash(fmt.Sprintf("could not add %s: %s", rule, err), "error")
            }
            session.AddFlash(fmt.Sprintf("adding %s", rule), "info")
		} 

		// todo: clean up manticore based on new rule
		//manticoreClient.RemoveByBaseUrl(urlData.BaseUrl)
		session.Save()
	}

	c.Redirect(http.StatusFound, "/blocklist")
}

func AddBlockPost(c *gin.Context) {
	log.Debug().Msg("calling add rule")
	url := c.PostForm("block_rule")
	addToBlocklist(c, url)
}

func AddBlockGet(c *gin.Context) {
	log.Debug().Msg("calling add rule")
	url := c.Query("block_rule")
	addToBlocklist(c, url)
}

func RemoveBlock(c *gin.Context) {
	log.Debug().Msg("calling remove rule")
	session := sessions.Default(c)

	rule := c.Query("rule")
	redisWrapper.RmFromBlocklist(rule)
	session.AddFlash(fmt.Sprintf("removed %s from blocklist", rule), "info")
	session.Save()
	c.Redirect(http.StatusFound, "/blocklist")
}
