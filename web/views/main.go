package views

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"codeberg.org/meatpuppet/soup/manticore"
	"codeberg.org/meatpuppet/soup/redis"
)

var manticoreClient = manticore.ClientWrapper()
var redisWrapper = redis.Wrapper()


type Flashes struct {
    Info []interface{}
    Warning []interface{}
    Error []interface{}
}

func getFlashes(c *gin.Context) *Flashes {
	session := sessions.Default(c)
    flashes := Flashes{
        Info: session.Flashes("info"),
        Warning: session.Flashes("warning"),
        Error: session.Flashes("error"),
    }
    session.Save()
    return &flashes
}
