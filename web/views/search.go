package views

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

var (
    mainFallback string
    mainFallbackName string
)


func init() {
    mainFallback = "https://duckduckgo.com/?q=%s"
    mainFallbackName = "duckduckgo"
    _, err := url.Parse(mainFallback)
    if err != nil {
        log.Panic().Msgf("fallback search '%s' does not look right", mainFallback)
    }
}


func Search(c *gin.Context) {
    engines := map[string]string {
        "ddg": "https://duckduckgo.com/?q=%s",
        "hubgrep": "https://duckduckgo.com/?q=%s",
        "pt": "https://search.joinpeertube.org/search?search=%s",
        "youtube": "https://www.youtube.com/results?search_query=%s",
        "wiki": "https://www.wikiwand.com/en/%s",
        "wikide": "https://www.wikiwand.com/de/%s",
        "reddit": "https://www.reddit.com/search/?q=%s",
        "libgen": "http://libgen.rs/search.php?req=%s",
        //"libgen.rs, libgen.is, libgen.st." 
        //"lemmy": "",
    }

	searchPhrase := c.DefaultQuery("phrase", "")
    searchPhrase = strings.TrimSpace(searchPhrase)
    
    for key, urlTemplate := range engines {
        if strings.HasPrefix(searchPhrase, key + " ") {
            searchPhrase = strings.TrimPrefix(searchPhrase, key + " ")
            c.Redirect(http.StatusTemporaryRedirect, fmt.Sprintf(urlTemplate, searchPhrase))
        }
    }

    mainFallbackSearch := fmt.Sprintf(mainFallback, searchPhrase)

	results := manticoreClient.Search(searchPhrase, "")
	c.HTML(http.StatusOK, "search.tmpl.html", gin.H{
		"results":      results,
		"searchPhrase": searchPhrase,
        "fallbackSearch": mainFallbackSearch,
        "fallbackName": mainFallbackName,
	})
}



