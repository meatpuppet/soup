package views

import (
	"codeberg.org/meatpuppet/soup/config"
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)


func init() {
	for _, id := range config.Config.GetQueueIds() {
        id := id
		promauto.NewGaugeFunc(prometheus.GaugeOpts{
            Name: "soup_queue_length",
            Help: "Number of urls in todo list",
            ConstLabels: prometheus.Labels{
                "queue_id": id,
            },
        }, func() float64 {
			return float64(redisWrapper.TodoLength(id))
		})
	}
}

func MetricsHandler() gin.HandlerFunc {
	h := promhttp.Handler()

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}
