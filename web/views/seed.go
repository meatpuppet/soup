package views

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func Seed(c *gin.Context) {
	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		page = 1
	}
	perPage, err := strconv.Atoi(c.DefaultQuery("perPage", "100"))
	if err != nil {
		perPage = 100
	}
	filterPattern := c.DefaultQuery("filter", "")

	seedUrls := manticoreClient.GetSeedUrls(page, perPage, filterPattern)

	c.Request.URL.ForceQuery = true
	query := c.Request.URL.Query()
	query.Set("page", fmt.Sprintf("%d", (page+1)))
	c.Request.URL.RawQuery = query.Encode()
	nextUrl := c.Request.URL.String()

	prevUrl := ""
	if page > 1 {
		c.Request.URL.Query().Set("page", fmt.Sprintf("%d", page-1))
		query.Set("page", fmt.Sprintf("%d", (page-1)))
		c.Request.URL.RawQuery = query.Encode()
		prevUrl = c.Request.URL.String()
	}

	c.HTML(http.StatusOK, "seeds.tmpl.html", gin.H{
		"page":          page,
		"perPage":       perPage,
		"filterPattern": filterPattern,
		"nextUrl":       nextUrl,
		"prevUrl":       prevUrl,
		"seedUrls":      seedUrls,
		"flashes":       getFlashes(c),
	})
}
