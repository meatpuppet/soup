package views

import (
	"fmt"
	"strconv"
	"strings"

	"codeberg.org/meatpuppet/soup/config"
	"codeberg.org/meatpuppet/soup/urldata"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"

	"net/http"
)

func AddSeed(c *gin.Context) {
	log.Debug().Msg("calling add seed")
	session := sessions.Default(c)

	seedUrl := c.PostForm("seed_url")
    seedUrl = strings.TrimSpace(seedUrl)
	comment := c.PostForm("comment")
    
    stepsstr := c.DefaultPostForm("steps", "")
    if stepsstr == "" {
        stepsstr = "1"
    }
    log.Info().Msgf("adding with %s steps", stepsstr)
	steps, err := strconv.Atoi(stepsstr)
    if err != nil {
        session.AddFlash(fmt.Sprintf("steps must be a number!: %s", err), "error")
        session.Save()
        c.Redirect(http.StatusFound, "/seed")
        return
    }
    if steps > config.Config.MaxSteps {
        session.AddFlash(fmt.Sprintf("steps must be < %d", config.Config.MaxSteps), "error")
        session.Save()
        c.Redirect(http.StatusFound, "/seed")
        return
    }

	urlData, err := urldata.NewUrlData(seedUrl, 1, steps)
	if err != nil {
		log.Warn().Msgf("error parsing %s: %s", seedUrl, err)
		session.AddFlash(fmt.Sprintf("error parsing url: %s", err), "error")
    // resolve forwarding
	} else if err = urlData.TryUpdateUrl(); err != nil {
		log.Warn().Msgf("error getting %s: %s", seedUrl, err)
		session.AddFlash(fmt.Sprintf("error getting %s: %s", seedUrl, err), "error")
	} else {
		manticoreClient.AddSeedUrl(urlData, 1, steps, comment)
		log.Info().Msgf("%s added to the seed", urlData.Url)
		session.AddFlash(fmt.Sprintf("%s was added to the seed urls!", urlData.Url), "info")
		redisWrapper.AddToTodo(urlData, false)
	}
	session.Save()
	c.Redirect(http.StatusFound, "/seed")
}

func RemoveSeed(c *gin.Context) {
	log.Debug().Msg("calling remove seed")
	session := sessions.Default(c)
	seedId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Error().Msg(fmt.Sprintf("%s", err))
		session.AddFlash("invalid seed id", "error")
	}

	err = manticoreClient.RemoveSeedUrl(seedId)
	if err != nil {
		log.Error().Msg(fmt.Sprintf("%s", err))
		session.AddFlash("error deleting seed url", "error")
	}

	session.Save()
	c.Redirect(http.StatusFound, "/seed")
}
