package views

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Blocklist(c *gin.Context) {
	blocklist := redisWrapper.GetBlocklist()

	c.HTML(http.StatusOK, "blocklist.tmpl.html", gin.H{
		"Blocklist": blocklist,
		"flashes":   getFlashes(c),
	})
}
