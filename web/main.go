package web

import (
	"fmt"
	"html/template"
	"time"

    //"codeberg.org/meatpuppet/soup/config"
	"codeberg.org/meatpuppet/soup/web/views"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	//"github.com/utrack/gin-csrf"
)

func formatAsDate(t time.Time) string {
	year, month, day := t.Date()
	return fmt.Sprintf("%d%02d/%02d", year, month, day)
}

func safe(s string) template.HTML {
    return template.HTML(s)
}

func Serve() {
	router := gin.Default()

	store := cookie.NewStore([]byte("secret"))
	router.Use(sessions.Sessions("soup_session", store))
	/*router.Use(csrf.Middleware(csrf.Options{
		Secret: config.Config.SecretKey,
		ErrorFunc: func(c *gin.Context) {
			c.String(400, "CSRF token mismatch")
			c.Abort()
		},
	}))
	*/
    //router.Delims("{[{", "}]}")
    //
	router.SetFuncMap(template.FuncMap{
		"formatAsDate": formatAsDate,
        "safe": safe,
	})
	router.LoadHTMLGlob("./web/templates/*.tmpl.html")
	router.Static("/static/", "./web/static/")

	router.GET("/", views.Index)
	router.GET("/search", views.Search)

	router.GET("/crawled", views.CrawledDomains)
	router.POST("/crawled", views.CrawledDomains)

	router.GET("/seed", views.Seed)
	router.POST("/add_seed", views.AddSeed)
    router.GET("/remove_seed/:id", views.RemoveSeed)
	
    router.GET("/blocklist", views.Blocklist)
	router.POST("/add_block_rule", views.AddBlockPost)
	router.GET("/add_block_rule", views.AddBlockGet)
    router.GET("/remove_block_rule", views.RemoveBlock)
    
    router.GET("/metrics", views.MetricsHandler())


	router.Run(":8080")
}


