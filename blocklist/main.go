package blocklist

import (
    "regexp"
	"codeberg.org/meatpuppet/soup/redis"
	"github.com/rs/zerolog/log"
)

type blockRules struct {
    rules []*regexp.Regexp
}

var BlockRules blockRules

func init() {
    loadBocklist()
}

func (rules *blockRules) WaitForUpdate(updateChannel <-chan string) {
	for {
		select {
		case rule := <-updateChannel:
            rules.addToLocalBlocklist(rule)
		}
	}
}

func (rules *blockRules) addToLocalBlocklist(rule string) error {
    parsedRule, err := regexp.Compile(rule)
    if err != nil {
        return err
    }
    BlockRules.rules = append(BlockRules.rules, parsedRule)
    log.Debug().Msgf("adding rule %s", rule)
    return nil
}

func (rules *blockRules) AddToBlocklist(rule string) error {
    err := rules.addToLocalBlocklist(rule)
    if err!= nil {
        return err
    }

    redis.Wrapper().AddToBlocklist(rule)
    return err
}

func (rules *blockRules) IsBlocked(url string) bool {
    for _, rule := range BlockRules.rules {
        match := rule.Match([]byte(url))
        if match {
            return true
        }
    }
    return false
}

func loadBocklist() {
	for _, line := range redis.Wrapper().GetBlocklist() {
        err := BlockRules.addToLocalBlocklist(line)
        if err!= nil {
            log.Error().Msgf("cannot add block rule '%s' - %s", line, err)
        }
	}
}
