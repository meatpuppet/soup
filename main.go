/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import "codeberg.org/meatpuppet/soup/cmd"

func main() {
	cmd.Execute()
}
