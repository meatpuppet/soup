package detectlanguage

import (
	"strings"

	"codeberg.org/meatpuppet/soup/config"
	"github.com/pemistahl/lingua-go"
	"github.com/rs/zerolog/log"
)

var detector lingua.LanguageDetector

func init() {
    // todo: make configurable
    
    languages := []lingua.Language{}
    
    for _, language := range lingua.AllLanguages() {
        for _, languageIso := range config.Config.Languages {
            if language.IsoCode639_1().String() == strings.ToUpper(languageIso) {
                languages = append(languages, language)
            }
        }
    }
    log.Debug().Msgf("starting with languages: %v", languages)

    detector = lingua.NewLanguageDetectorBuilder().
        FromLanguages(languages...).
        Build()
}

func DetectLanguage(text string) (string, bool) {
    language, canDetect := detector.DetectLanguageOf(text)
    if !canDetect {
        return "", false
    } else {
        return strings.ToLower(language.IsoCode639_1().String()), true
    }
}

