package redis

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"codeberg.org/meatpuppet/soup/urldata"
)

// add to todo, if not already done or in todo
// internal links will be lpushed, meaning they will be processed next - hopefully while we are still processing links from that host
func (w RedisWrapper) AddToTodo(urlData *urldata.UrlData, isInternal bool) string {
	if w.isDone(urlData.GetUrlHashStr()) {
        //log.Debug().Msgf("not adding - %s is done", urlData.Url)
		// is done already
		return ""
	}
	if w.isTodo(urlData.GetUrlHashStr()) {
		// is in todo already
        //log.Debug().Msgf("not adding - %s is in todo", urlData.Url)
		return ""
	}
    log.Debug().Msgf("adding %s", urlData.Url)
	w.setIsTodo(urlData)
    if isInternal {
        return w.lPushToTodoList(urlData)
    } else {
        return w.rPushToTodoList(urlData)
    }
}

// remove from todo keys, add to done keys
func (wrapper RedisWrapper) MoveToDone(urlData *urldata.UrlData) {
	wrapper.setIsDone(urlData.GetUrlHashStr())
	wrapper.rmTodo(urlData)
}

// return true if this urlhash exists in todo keys
func (wrapper RedisWrapper) isDone(urlHash string) bool {
	key := fmt.Sprintf("%s%s", donePrefix, urlHash)
	isDone := wrapper.client.Get(key)

    // todo: track if we processed external links here?
    // would help dealing with "cut off" paths (if we processed this page as "the last step"
    // we put it to done. when we find it again on a shorter path we want to process the external links too, but we already marked it done, skipping it
    // maybe add a second value "2" for processed, but only interal links?

	if isDone.Val() == "1" {
		return true
	}
	return false
}

func (wrapper RedisWrapper) setIsDone(urlHash string) {
	key := fmt.Sprintf("%s%s", donePrefix, urlHash)
	wrapper.client.Set(key, "1", 0)
}

func (wrapper RedisWrapper) isTodo(urlHash string) bool {
	key := fmt.Sprintf("%s%s", todoPrefix, urlHash)
	_, err := wrapper.client.Get(key).Result()
	if err != nil {
		return false
	}
	return true
}

func (w RedisWrapper) lPushToTodoList(urlData *urldata.UrlData) string {
	log.Debug().Msgf("lpushing to todo list: %x", urlData.UrlHash)

    queueId := urlData.GetQueueId()
    todoListKey := fmt.Sprintf("%s%s", todoList, queueId)
    log.Debug().Msgf("adding to todo list %s", todoListKey)
	_, err := w.client.LPush(todoListKey, urlData.GetUrlHashStr()).Result()
	if err != nil {
		panic(err)
	}
    return queueId
}
func (w RedisWrapper) rPushToTodoList(urlData *urldata.UrlData) string {
	log.Debug().Msgf("rpushing to todo list: %x", urlData.UrlHash)

    queueId := urlData.GetQueueId()
    todoListKey := fmt.Sprintf("%s%s", todoList, queueId)
    log.Debug().Msgf("adding to todo list %s", todoListKey)
	_, err := w.client.RPush(todoListKey, urlData.GetUrlHashStr()).Result()
	if err != nil {
		panic(err)
	}
    return queueId
}

func (wrapper RedisWrapper) PopFromTodoList(queueId string) (string, error) {
    todoListKey := fmt.Sprintf("%s%s", todoList, queueId)
    log.Debug().Msgf("popping from %s", todoListKey)
	val, err := wrapper.client.LPop(todoListKey).Bytes()
	if err != nil {
		return "", err
	}
    log.Debug().Msgf("popped %s (len %d)", val, len(val))

	return string(val), nil
}

func (wrapper RedisWrapper) TodoLength(queueId string) int {
    todoListKey := fmt.Sprintf("%s%s", todoList, queueId)
    length, err := wrapper.client.LLen(todoListKey).Result()
    if err != nil {
        panic(err)
    }
    return int(length)
}

func (w RedisWrapper) setIsTodo(urlData *urldata.UrlData) {
	log.Debug().Msgf("setting todo data: %+v\n", urlData)
	key := fmt.Sprintf("%s%s", todoPrefix, urlData.GetUrlHashStr())
	_, err := w.client.Set(key, urlData.ToTodoJson(), 0).Result()
	if err != nil {
		panic(err)
	}
}

func (w RedisWrapper) GetTodoUrlData(urlHash string) (*urldata.UrlData, error) {
	key := fmt.Sprintf("%s%s", todoPrefix, urlHash)
	val, err := w.client.Get(key).Result()
	if err != nil {
        log.Error().Msgf("cant find data for %s in redis", urlHash)
        return nil, err
	}
	urlData, err := urldata.UrlDataFromTodoJson([]byte(val))
	if err != nil {
        return nil, err
	}
	return urlData, err
}

// remove todo key for url
func (wrapper RedisWrapper) rmTodo(urlData *urldata.UrlData) {
	key := fmt.Sprintf("%s%s", todoPrefix, urlData.GetUrlHashStr())
	wrapper.client.Del(key)
}

