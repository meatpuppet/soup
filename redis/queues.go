package redis

import (
	"codeberg.org/meatpuppet/soup/config"
)


func (wrapper *RedisWrapper) AllQueuesEmpty() bool {
	for _, id := range config.Config.GetQueueIds() {
		if wrapper.TodoLength(id) != 0 {
			return false
		}
	}
	return true
}
