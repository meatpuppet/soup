package redis

import (
	"codeberg.org/meatpuppet/soup/urldata"
	"testing"
    "log"
)

func setupSuite(tb testing.TB) func(tb testing.TB) {

	// Return a function to teardown the test
	return func(tb testing.TB) {
		log.Println("teardown suite")
        wrapper := Wrapper()
        wrapper.FlushDb()
	}
}

func TestNew(t *testing.T) {
    teardown := setupSuite(t)
    defer teardown(t)

	url := "http://someurl/"
	urlData, err := urldata.NewUrlData(url, 1, 1)
	if err != nil {
		t.Log("making urldata should not error")
		t.Log(err.Error())
		t.Fail()
	}
	if urlData.Url != url {
		t.Log("blatest")
		t.Fail()
	}
}

