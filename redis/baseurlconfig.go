package redis

import (
	"fmt"
	"codeberg.org/meatpuppet/soup/urldata"

)

func (w RedisWrapper) SetBaseUrlConfig(urlData *urldata.UrlData, baseUrlConfig *urldata.BaseUrlConfig) error {
	key := fmt.Sprintf("%s%s", baseUrlConfigPrefix, urlData.BaseUrl)
	_, err := w.client.Set(key, baseUrlConfig.ToBaseUrlConfigJson(), 0).Result()
	if err != nil {
		panic(err)
	}
	return nil
}

func (w RedisWrapper) GetBaseUrlConfig(urlData *urldata.UrlData) *urldata.BaseUrlConfig {
	key := fmt.Sprintf("%s%s", baseUrlConfigPrefix, urlData.BaseUrl)
	val, err := w.client.Get(key).Result()

	if err != nil {
		// maybe we just dont have a config for this base url - this should be the norm, actually..
		// log.Info().Msgf("no base url config found for %s - defaulting", urlData.BaseUrl)
		return urldata.DefaultBaseUrlConfig()
	}

	baseUrlConfig := urldata.BaseUrlConfigFromJson([]byte(val))
	return baseUrlConfig
}

func (w *RedisWrapper) getAllBaseUrlConfigs() *map[string]*urldata.BaseUrlConfig {
	baseUrlConfigs := map[string]*urldata.BaseUrlConfig{}

	for key, value := range *w.getAllPrefix(baseUrlConfigPrefix) {
		baseUrlConfigs[key] = urldata.BaseUrlConfigFromJson([]byte(value))
	}
	return &baseUrlConfigs
}
