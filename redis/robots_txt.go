package redis

import (
	"fmt"
	"codeberg.org/meatpuppet/soup/crawler/robots_txt"
	"codeberg.org/meatpuppet/soup/urldata"
)


func (w *RedisWrapper) SetRobotsTxt(urlData *urldata.UrlData, robotsTxt *robotstxt.RobotsTxt) {
	key := fmt.Sprintf("%s%s", robotsTxtPrefix, urlData.GetBaseUrlHashStr())
	_, err := w.client.Set(key, robotsTxt.RobotsTxtData, 0).Result()
	if err != nil {
		panic(err)
	}
}

func (w *RedisWrapper) GetRobotsTxt(urlData *urldata.UrlData) (*robotstxt.RobotsTxt, error) {
	key := fmt.Sprintf("%s%s", robotsTxtPrefix, urlData.GetBaseUrlHashStr())
	val, err := w.client.Get(key).Result()
	if err != nil {
        // maybe we just dont have a config for this base url - this should be the norm, actually..
		return nil, err
	}

    robotsTxt := robotstxt.FromBytes([]byte(val))
	if err != nil {
        // we should be able to parse this - otherwise something is wrong...
		panic(err)
	}
	return robotsTxt, nil

}

