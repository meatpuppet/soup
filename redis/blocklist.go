package redis

import (
	"github.com/rs/zerolog/log"
)


func (w *RedisWrapper) publishBlocklistUpdate(rule string) {
    w.client.Publish(blocklistKey, rule)
}

func (w *RedisWrapper) SubscribeBlocklistUpdate(rulesChannel chan<- string) {
    pubsub := w.client.Subscribe(blocklistKey)
    for m := range pubsub.Channel() {
        log.Warn().Msg("got blocklist update message")
        rulesChannel <- m.Payload
    }
}

func (w *RedisWrapper) GetBlocklist() ([]string) {
    res := w.client.SMembers(blocklistKey)
    return res.Val()
}

func (wrapper RedisWrapper) AddToBlocklist(rule string) error {
    log.Debug().Msgf("adding to blocklist: %s", rule)
    wrapper.publishBlocklistUpdate(rule)
	wrapper.client.SAdd(blocklistKey, rule)
    return nil
}

func (wrapper RedisWrapper) RmFromBlocklist(rule string) {
    log.Debug().Msgf("removing from blocklist: %s", rule)
	wrapper.client.SRem(blocklistKey, rule)
}
