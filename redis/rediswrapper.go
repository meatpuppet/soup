package redis

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"codeberg.org/meatpuppet/soup/config"

	"github.com/go-redis/redis"
	"github.com/rs/zerolog/log"
)

// a mapping of done:<urlhash> to "1" to check if a hash is already processed
var donePrefix = "done:"

// mapping todo:<urlhash> to UrlData
var todoPrefix = "todo:"

// mapping baseurlconf:<baseurl> to config.BaseUrlConfig
var baseUrlConfigPrefix = "baseurlconf:"

// mapping seed urls robots:<baseurl> to robotsTxt
var robotsTxtPrefix = "robots:"

// mapping tier:<baseUrlHash> to int tier
var baseUrlTierPrefix = "tier:"

// mapping of budget:<baseUrlHash> to the budget the base url already used (int)
var usedBudgetPrefix = "budget:"

var blocklistKey = "blocklist"

// something that keeps order, a list of urlhashes
// this gets the queue id appended, so there is actually a bunch of lists
// "todolist:<baseurlhash[0]>" -> [urlhash, urlhash, ...]
var todoList = "todolist:"

type RedisWrapper struct {
	client *redis.Client // a redis client
}

// get new wrapper instance
func Wrapper() *RedisWrapper {
	log.Debug().Msgf("connecting to redis at %s/%d", config.Config.RedisAddress, config.Config.RedisDB)
	client := redis.NewClient(&redis.Options{
		Addr:     config.Config.RedisAddress,
		Password: config.Config.RedisPassword,
		DB:       config.Config.RedisDB,
	})
	return &RedisWrapper{client}
}

func (wrapper RedisWrapper) FlushDb() {
	_, err := wrapper.client.FlushDB().Result()
	if err != nil {
		panic(err)
	}
}

// clean working data before starting a new run
func (w *RedisWrapper) CleanDb() {
	for _, prefix := range []string{donePrefix, todoPrefix, robotsTxtPrefix, baseUrlTierPrefix} {
		log.Info().Msg("getting " + prefix)
		cursor := uint64(0)
		for {
			cursor, d := w.getPrefixPage(prefix, "", cursor, 1000, false)
			for k := range *d {
				w.client.Del(k).Result()
			}
			if cursor == 0 {
				log.Debug().Msg("hit cursor")
				break
			}
		}
	}

	for id := range config.Config.GetQueueIds() {
		key := fmt.Sprintf("%s%d", todoList, id)
		w.client.Del(key)
	}
}

func (w *RedisWrapper) getPrefixPage(prefix string, filterPattern string, cursor uint64, size int64, trimPrefix bool) (uint64, *map[string]string) {
	data := map[string]string{}

	var allKeys []string
	var keys []string
	var err error
	for {
		keys, cursor, err = w.client.Scan(cursor, fmt.Sprintf("%s%s*", prefix, filterPattern), size).Result()
		if err != nil {
			panic(err)
		}
		for _, k := range keys {
			allKeys = append(allKeys, k)
		}
		if len(allKeys) > int(size) {
			break
		}
		if cursor == 0 {
			break
		}
	}

	// get values for the keys
	for _, key := range allKeys {
		val, err := w.client.Get(key).Result()
		if err != nil {
			log.Error().Msgf("error getting key: %s", err)
			continue
		}
		if trimPrefix {
			data[strings.TrimPrefix(key, prefix)] = val
		} else {
			data[key] = val
		}
	}
	return cursor, &data
}

func (w *RedisWrapper) getAllPrefix(prefix string) *map[string]string {
	data := map[string]string{}
	var cursor uint64

	for {
		cursor, d := w.getPrefixPage(prefix, "", cursor, 100, false)
		for k, v := range *d {
			data[k] = v
		}
		if cursor == 0 {
			break
		}
	}
	return &data
}

func shouldBeBackedUp(key string) bool {
	prefixes := []string{baseUrlConfigPrefix}
	for _, prefix := range prefixes {
		if strings.HasPrefix(key, prefix) {
			return true
		} else {
			log.Debug().Msgf("%s != %s", key, prefix)
		}
	}
	return false
}

func (w *RedisWrapper) Backup() {
	// create file
	f, err := os.Create("backup/exclude.txt")
	if err != nil {
		log.Fatal().Msgf("%s", err)
	}
	// remember to close the file
	defer f.Close()

	// create new buffer
	buffer := bufio.NewWriter(f)

	for _, line := range w.GetBlocklist() {
        log.Info().Msgf(line)
		_, err := buffer.WriteString(line + "\n")
		if err != nil {
			log.Fatal().Msgf("%s", err)
		}
	}
    buffer.Flush()
}
