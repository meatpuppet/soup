package redis

import (
	"testing"
	"codeberg.org/meatpuppet/soup/urldata"
)

func TestIsTodo(t *testing.T) {
	teardown := setupSuite(t)
	defer teardown(t)
	wrapper := Wrapper()

	// test valid data
	urlData, _ := urldata.NewUrlData("http://url", 1, 1)
	wrapper.setIsTodo(urlData)
	isTodo := wrapper.isTodo(urlData.GetUrlHashStr())
	if isTodo != true {
		t.Log("should find the hash we just stored")
		t.Fail()
	}

	// test something not in redis
	fakeHash := urldata.GetHexString(1)
	isTodo = wrapper.isTodo(fakeHash)
	if isTodo != false {
		t.Log("shouldnt find made up hashes")
		t.Fail()
	}
}

func TestAddToTodoList(t *testing.T) {
	teardown := setupSuite(t)
	defer teardown(t)
	wrapper := Wrapper()
	
    urlData, _ := urldata.NewUrlData("http://url.tld", 1, 1)
    queueId := wrapper.rPushToTodoList(urlData)
	
    differentUrlData, _ := urldata.NewUrlData("http://differenturasdfl.tld", 1, 1)
    differentQueueId := wrapper.rPushToTodoList(differentUrlData)

    if queueId == differentQueueId {
        t.Logf("urls should be in different queues!")
        t.Fail()
    }
	
    popped, _ := wrapper.PopFromTodoList(queueId)
	if popped != urlData.GetUrlHashStr() {
        t.Logf("popped unexpected hash!")
		t.Fail()
	}
    popped, err := wrapper.PopFromTodoList(queueId)
	if err == nil {
        t.Logf("queue should have been empty!")
		t.Fail()
	}

    popped, _ = wrapper.PopFromTodoList(differentQueueId)
	if popped != differentUrlData.GetUrlHashStr() {
        t.Logf("popped unexpected hash for second urldata!")
		t.Fail()
	}
}

func TestAddToTodo(t *testing.T) {
	teardown := setupSuite(t)
	defer teardown(t)
	urlData, _ := urldata.NewUrlData("http://url", 1, 1)
	wrapper := Wrapper()
	wrapper.rPushToTodoList(urlData)
}

func TestTodoLength(t *testing.T) {
	teardown := setupSuite(t)
	defer teardown(t)
	urlData, _ := urldata.NewUrlData("http://url", 1, 1)
	wrapper := Wrapper()
	wrapper.rPushToTodoList(urlData)

	length := wrapper.TodoLength(urlData.GetQueueId())
	if length != 1 {
		t.Logf("length was %d", length)
		t.Fail()
	}

	length = wrapper.TodoLength("notaqueue")
	if length != 0 {
		t.Logf("length was %d", length)
		t.Fail()
	}
}

func TestAddToTodoListOrder(t *testing.T) {
    expected2 := "http://test.tld/external1"
    expected1 := "http://test.tld/internal"
    expected3 := "http://test.tld/external2"

    urlData, _ := urldata.NewUrlData(expected2, 1, 1)
    Wrapper().AddToTodo(urlData, false)
    
    // internal should pop first, no matter when added
    urlData, _ = urldata.NewUrlData(expected1, 1, 1)
    queueId := Wrapper().AddToTodo(urlData, true)

    urlData, _ = urldata.NewUrlData(expected3, 1, 1)
    queueIdExternal := Wrapper().AddToTodo(urlData, false)

    if queueId != queueIdExternal {
        t.Logf("queues should have the same id")
        t.Fail()
    }
   

    expected := []string{expected1, expected2, expected3}
    
    for _, expectedResult := range expected {
        urlHash, _ := Wrapper().PopFromTodoList(queueId) 
        urlData, _ := Wrapper().GetTodoUrlData(urlHash)
        if expectedResult != urlData.Url {
            t.Logf("expected %s, got %v", expectedResult, urlData.Url)
        }
    }
}
