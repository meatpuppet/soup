package redis

import (
    "fmt"
    "codeberg.org/meatpuppet/soup/urldata"
)

func (wrapper RedisWrapper) IncreaseUsedBudget(urlData *urldata.UrlData) int {
	key := fmt.Sprintf("%s%s", usedBudgetPrefix, urlData.GetBaseUrlHashStr())
	usedBudget := wrapper.client.Incr(key).Val()
	return int(usedBudget)
}

