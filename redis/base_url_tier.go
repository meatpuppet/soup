package redis

import (
	"fmt"
	"codeberg.org/meatpuppet/soup/urldata"
	"strconv"
)

func (w *RedisWrapper) SetBaseUrlTier(urlData *urldata.UrlData, tier int) {
	key := fmt.Sprintf("%s%s", baseUrlTierPrefix, urlData.GetBaseUrlHashStr())
	_, err := w.client.Set(key, tier, 0).Result()
	if err != nil {
		panic(err)
	}
}

func (w *RedisWrapper) GetBaseUrlTier(urlData *urldata.UrlData) (int, error) {
	key := fmt.Sprintf("%s%s", baseUrlTierPrefix, urlData.GetBaseUrlHashStr())
	val, err := w.client.Get(key).Result()
	if err != nil {
        // maybe we just dont have a config for this base url - this should be the norm, actually..
		return -1, err
	}

    tier, err := strconv.Atoi(val)
	if err != nil {
        // we should be able to parse this - otherwise something is wrong...
		panic(err)
	}
	return tier, nil
}

